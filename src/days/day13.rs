use crate::util::{self, intcode};
use std::collections::HashMap;
use std::cmp;
use num_derive::{FromPrimitive, ToPrimitive};
use num_traits::{FromPrimitive, ToPrimitive};

#[derive(Eq, PartialEq, Hash, Copy, Clone)]
struct Coord { x: u32, y: u32 }
impl Coord { fn new() -> Coord { Coord{x:0,y:0} } }

#[derive(FromPrimitive, ToPrimitive, Copy, Clone)]
enum Tile {
  EMPTY = 0,
  WALL,
  BLOCK,
  PADDLE,
  BALL
}

struct Field {
  tiles: HashMap<Coord, Tile>,
  counts: [u32; 5],
  size: Coord,
  ball: Coord,
  paddle: Coord,
  score: u32,
}
impl Field {
  fn new() -> Field {
    Field {
      tiles: HashMap::new(),
      counts: [0,0,0,0,0],
      size: Coord::new(),
      ball: Coord::new(),
      paddle: Coord::new(),
      score: 0,
    }
  }

  fn set_tile(&mut self, loc: Coord, tile: u8) {
    let tile = FromPrimitive::from_u8(tile).unwrap();
    match self.tiles.get_mut(&loc) {
      Some(t) => {
        self.counts[t.to_usize().unwrap()] -= 1;
        *t = tile;
      },
      None => {self.tiles.insert(loc, tile);},
    }
    self.counts[tile as usize] += 1;
    self.size.x = cmp::max(self.size.x, loc.x+1);
    self.size.y = cmp::max(self.size.y, loc.y+1);

    match tile {
      Tile::PADDLE => self.paddle = loc,
      Tile::BALL => self.ball = loc,
      _ => ()
    }
  }

  fn set_score(&mut self, score: u32) {
    self.score = score;
  }

  fn draw(&self) -> String {
    let mut out = String::new();
    out.push_str(&format!("Score: {}\n", self.score));
    for y in 0..self.size.y {
      for x in 0..self.size.x {
        out.push(match self.tiles.get(&Coord{x, y}) {
          Some(id) => match id {
            Tile::EMPTY => ' ',
            Tile::WALL => '█',
            Tile::BLOCK => '▒',
            Tile::PADDLE => '▃',
            Tile::BALL => '*',
          },
          None => 'X',
        })
      }
      out.push('\n');
    }
    out
  }
}

pub fn run(input: String, part: Option<u8>) -> String {
  let mut prog = intcode::Program::from_csv(&input);

  prog.reset();
  let field = read_screen(&mut prog);
  println!("{}{}", util::cls(), field.draw());

  let mut score = 0;
  if part == Some(2) {
    prog.reset();
    prog.set(0, 2);
    let mut last_ball: i128 = 0;
    let mut joy_dir: i128 = 0;
    while prog.state != intcode::ProgState::DONE {
      prog.put(joy_dir);
      prog.resume();
      let field = read_screen(&mut prog);
      //println!("{}{}", util::cls(), field.draw());

      let paddle_target = field.ball.x as i128;

      joy_dir = (paddle_target - field.paddle.x as i128).signum();
      last_ball = field.ball.x as i128;
      if score != field.score {
        println!("{}", score);
      }
      score = field.score;
    }
  }

  match part {
    Some(1) => field.counts[2].to_string(),
    Some(2) | None => score.to_string(),
    Some(p) => panic!("Unknown part {}!", p)
  }
}

fn read_screen(prog: &mut intcode::Program) -> Field {
  let mut field = Field::new();
  prog.resume();
  for chunk in prog.output.chunks(3) {
    if chunk[0] == -1 && chunk[1] == 0 {
      field.set_score(chunk[2] as u32);
    } else {
      field.set_tile(
        Coord{
          x: chunk[0] as u32,
          y: chunk[1] as u32
        },
        chunk[2] as u8
      );
    }
  }
  field
}
#[cfg(test)]
mod test {
}
