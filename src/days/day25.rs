use crate::util::{intcode, tilemap, nums};
use crate::util::geom::{Vec2, Direction};
use regex::Regex;

pub fn run(input: String, part: Option<u8>) -> String {
  let mut console = intcode::Console::new(intcode::Program::from_csv(&input));
  let mut map = tilemap::TileMap::new();

  let script = vec![
    "east", "take mug",
    "west", "west", "west", "north", "take fuel cell",
    "south", "take astrolabe",
    "south", "take hologram",
    "west", "east", "north", "east", "take ornament",
    "east", "east", "north", "take monolith",
    "south", "south", "west", "north", "west", "take bowl of rice",
    "north", "west",
    "drop bowl of rice",
    "drop monolith",
    "drop mug",
  ];

  let mut items = vec![
    "mug",
    "fuel cell",
    "astrolabe",
    "hologram",
    "ornament",
    "monolith",
    "bowl of rice",
    "north",
  ];

  for cmd in script {
    console.resume();
    console.send_command(cmd);
  }

  /*
  println!("Testing weight...");
  let items = find_item_combo(&mut console, &items);
  println!("Got in by dropping {:?}", items);
  */

  let mut pos = Vec2::new();
  map.set(pos, 'O');
  console.add_hook("command", Box::new(move |cmd| {
    match Direction::from_str(&cmd) {
      Some(dir) => {
        pos += dir.as_vec();
        if map.get(&pos) == None { map.set(pos, '.'); }
      },
      None => ()
    };
    map.set_overlay(vec![(pos, '#')]);
    println!("{}",map.drawc());
  }));
  console.run();

  match part {
    Some(1) => String::new(),
    Some(2) | None => String::new(),
    Some(p) => panic!("Unknown part {}!", p)
  }
}

fn find_item_combo<'a>(console: &mut intcode::Console, items: &Vec<&'a str>) -> Vec<&'a str> {
  let re = Regex::new(r"Alert").unwrap();
  for i in 0..items.len() {
    for remove in nums::choose_n(items, i) {
      debug!("Trying removing {:?}", remove);
      for it in &remove {
        console.send_command(&format!("drop {}", it));
        console.resume();
      }
      console.send_command("north");
      console.resume();
      if !re.is_match(&console.get_response()) {
        return remove;
      }
      for it in &remove {
        console.send_command(&format!("take {}", it));
        console.resume();
      }
    }
  }
  vec![]
}

#[cfg(test)]
mod test {
  use super::*;
}
