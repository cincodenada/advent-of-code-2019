type Row = Vec<u8>;
type Layer = Vec<Row>;
type Image = Vec<Layer>;

pub fn run(input: String, part: Option<u8>) -> String {
  let img = build_matrix(input.trim(), 25, 6);
  let mut min = std::u32::MAX;
  let mut min_layer = None;
  for layer in &img {
    match count_val(0, &layer) {
      cnt if cnt < min => {
        min = cnt;
        min_layer = Some(layer);
      },
      _ => {}
    }
  }
  
  match part {
    Some(1) => (count_val(1, min_layer.as_ref().unwrap()) * count_val(2, min_layer.as_ref().unwrap())).to_string(),
    Some(2) | None => {
      merge_layers(img).iter().map(
        |r| r.iter().map(
          |px| match px { 0 => " ", 1 => "█", p => panic!("Unexpected pixel value {}", p)}
        ).collect::<Vec<&str>>().join("")
      ).collect::<Vec<String>>().join("\n")
    },
    Some(p) => panic!("Unknown part {}!", p)
  }
}

fn count_val(digit: u8, layer: &Layer) -> u32 {
  layer.iter().map(|r| r.iter().map(|p| (*p == digit) as u32).sum::<u32>()).sum()
}

fn merge_layers(mut img: Image) -> Layer {
  let mut output = img.remove(0);

  for layer in img {
    for (y, row) in layer.iter().enumerate() {
      for (x, val) in row.iter().enumerate() {
        if output[y][x] == 2 { output[y][x] = *val; }
      }
    }
  }

  output
}

fn build_matrix(imgdata: &str, w: u32, h: u32) -> Image {
  // layer, row, column
  let mut image = Vec::new();
  let mut x = 0;
  let mut y = 0;

  let mut cur_layer = Vec::new();
  let mut cur_row = Vec::new();

  for p in imgdata.chars().map(|c| c.to_digit(10).unwrap() as u8) {
    cur_row.push(p);
    
    x += 1;
    if x >= w {
      cur_layer.push(cur_row);
      cur_row = Vec::new();
      y += 1;
      x = 0;
      if y >= h {
        image.push(cur_layer);
        cur_layer = Vec::new();
        y = 0;
      }
    }
  }

  image
}

#[cfg(test)]
mod test {
  use super::*;

  #[test]
  fn example() {
    assert_eq!(
      vec![
        vec![
          vec![1,2,3],
          vec![4,5,6],
        ],
        vec![
          vec![7,8,9],
          vec![0,1,2],
        ],
      ],
      build_matrix("123456789012", 3, 2)
    );
  }

  #[test]
  fn merge() {
    assert_eq!(
      vec![
        vec![0,1],
        vec![1,0],
      ],
      merge_layers(build_matrix("0222112222120000", 2, 2))
    );
  }
}
