use crate::util::intcode;

pub fn run(input: String, part: Option<u8>) -> String {
  let ints : Vec<i128> = input.trim().split(',').map(|s| s.parse::<i128>().unwrap()).collect();

  match part {
    Some(1) => run_prog(ints, 12, 2).to_string(),
    Some(2) | _ => {
      match find_output(&ints, 19690720) {
        Some(params) => (params.0*100+params.1).to_string(),
        None => panic!("No matching output found!")
      }
    }
  }
}

fn run_prog(mut ints: Vec<i128>, noun: i128, verb: i128) -> i128 {
  ints[1] = noun;
  ints[2] = verb;
  let mut prog = intcode::Program::from_vec(ints);
  prog.run();

  prog.at(0)
}

fn find_output(ints: &Vec<i128>, output: i128) -> Option<(i128, i128)> {
  for noun in 0..99 {
    for verb in 0..99 {
      let ints = ints.clone();
      match run_prog(ints, noun, verb) {
        out if out == output => return Some((noun, verb)),
        _ => ()
      }
    }
  }

  return None
}
