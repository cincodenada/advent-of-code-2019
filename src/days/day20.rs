use crate::util::tilemap;
use crate::util::geom::{Vec2, Vec3, Direction};
use std::collections::{HashMap, VecDeque};
use std::fs::File;
use std::io::prelude::*;
use std::fmt;

#[derive(Debug)]
struct NamedNode {
  dir: Direction,
  level_change: i32,
  name: [char;2],
  pos: Vec2,
}
impl fmt::Display for NamedNode {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    write!(f, "{}{}", self.name[0], self.name[1])
  }
}

struct FakeGraph {
  size: Vec2,
  nodes: Vec<(Vec2, Option<[char;2]>)>,
  edges: HashMap<Vec2, Vec<(Vec2, u32, i32)>>,
  last_x: Option<i32>,
  last_y: Vec<Option<i32>>,
  cur_row: i32,
  named_nodes: HashMap<Vec2, NamedNode>,
  unpaired_names: HashMap<[char;2], Vec2>,
  max: Vec2,
}
impl FakeGraph {
  // TODO: Requiring height at init is a bit icky
  fn new(size: (usize, usize)) -> FakeGraph {
    FakeGraph {
      size: Vec2::from_xy(size.0 as i32, size.1 as i32),
      nodes: Vec::new(),
      edges: HashMap::new(),
      cur_row: 0,
      last_x: None,
      last_y: {let mut v = Vec::with_capacity(size.0); v.resize(size.0, None); v},
      named_nodes: HashMap::new(),
      unpaired_names: HashMap::new(),
      max: Vec2::from_xy(size.0 as i32 - 3, size.1 as i32 - 3),
    }
  }

  fn add_named_node(&mut self, x: usize, y: usize, id: [char;2]) {
    let p = Vec2::from_xy(x as i32, y as i32);
    let (level_change, dir) = match (p.x, p.y) {
      (2, _) => (-1, Direction::WEST),
      (_, 2) => (-1, Direction::NORTH),
      (x, _) if x == self.max.x => (-1, Direction::EAST),
      (_, y) if y == self.max.y => (-1, Direction::SOUTH),
      // TODO: This direction won't quite work right
      (x, _) if x < self.size.x/2 => (1, Direction::WEST),
      (_, y) if y < self.size.y/2 => (1, Direction::NORTH),
      (x, _) => (1, Direction::EAST),
      (_, y) => (1, Direction::SOUTH),
    };
    trace!("Adding named node {}{} @ ({},{}) with direction of {} and level change of {}", id[0], id[1], p.x, p.y, dir, level_change);

    self.named_nodes.insert(p, NamedNode {
      dir,
      level_change,
      name: id,
      pos: p,
    });

    match self.unpaired_names.get(&id) {
      Some(other) => self.add_edge(p, *other, 1, level_change),
      None => { self.unpaired_names.insert(id, p); },
    }

    self.nodes.push((p, Some(id)));
    self.add_edges(p);
  }
  fn add_node(&mut self, x: usize, y: usize) {
    let p = Vec2::from_xy(x as i32, y as i32);
    self.nodes.push((p, None));
    self.add_edges(p);
  }
  fn remove_node(&mut self, node: &Vec2) {
    self.nodes.retain(|v| v.0 != *node);
    let to_tidy: Vec<_> = self.edges[node].iter().map(|(other, _, _)| other.clone()).collect();
    self.edges.remove(&node);
    for other in to_tidy {
      self.edges.get_mut(&other).unwrap().retain(|v| v.0 != *node);
      if self.edges[&other].len() == 0 {
        self.edges.remove(&other);
      }
    }
  }

  fn add_edges(&mut self, p: Vec2) {
    if p.y != self.cur_row { self.last_x = None; self.cur_row = p.y }

    match self.last_x {
      Some(x) => {
        let from = Vec2::from_xy(x, p.y);
        self.add_edge(from, p, from.manhattan_dist(&p), 0);
      },
      None => (),
    }
    match self.last_y.get(p.x as usize) {
      Some(ly) => match ly {
        Some(y) => {
          let from = Vec2::from_xy(p.x, *y);
          self.add_edge(from, p, from.manhattan_dist(&p), 0);
        },
        None => (),
      }
      None => (),
    }

    self.last_x = Some(p.x);
    self.last_y[p.x as usize] = Some(p.y);
  }
  fn add_edge(&mut self, from: Vec2, to: Vec2, dist: u32, level_change: i32) {
    // Add edge in both directions
    for (key, val, lev) in [(from, to, level_change), (to, from, -level_change)].iter() {
      match self.edges.get_mut(key) {
        Some(v) => v.push((*val, dist, *lev)),
        None => { self.edges.insert(*key, vec![(*val, dist, *lev)]); },
      }
    }
  }

  fn prune(&mut self) -> u32 {
    let mut removed = 0;
    for (node, id) in self.nodes.iter().cloned().collect::<Vec<_>>() {
      if id == None && self.edges[&node].len() == 1 {
        self.remove_node(&node);
        removed+=1;
      }
    }
    removed
  }
  fn prune_all(&mut self) -> u32 {
    let mut total_pruned = 0;
    loop {
      match self.prune() {
        0 => break,
        n => total_pruned += n,
      }
    }
    total_pruned
  }

  fn clear_last(&mut self, x: usize, y: usize) {
    self.last_y[x] = None;
    self.last_x = None;
  }

  fn dot_file(&self, filename: &str) {
    let mut point_ids = HashMap::new();
    let mut out = String::new();
    out.push_str("strict graph {\n");
    for (id, (pos, name)) in self.nodes.iter().enumerate() {
      out.push_str(&format!(
        "{} [ label = \"{}\", pos = \"{},{}!\" ]\n",
        id, match name { Some(n) => n.iter().collect(), None => String::new() }, pos.x, -pos.y
      ));
      point_ids.insert(pos, id);
    }
    out.push('\n');
    for (from, v) in self.edges.iter() {
      for (to, _, _) in v.iter() {
        out.push_str(&format!("{} -- {}\n", point_ids[from], point_ids[to]));
      }
    }
    out.push_str("}\n");

    let mut file = File::create(filename).unwrap();
    file.write_all(out.as_bytes());
  }

  fn shortest_path(&self, from: Vec2, to: Vec2, recurse: bool) -> Vec<(u32, Vec3)> {
    let from = Vec3::from_xyz(from.x, from.y, 0);
    let to = Vec3::from_xyz(to.x, to.y, 0);

    let mut shortest_path = HashMap::new();
    let mut search: VecDeque<(Vec3, _)> = VecDeque::new();
    let mut held = VecDeque::new();
    let mut max_level = 0;
    search.push_front((from, 0));
    shortest_path.insert(from, (0, None));

    let map = self.as_map();

    let desired = ["XF","XF","CK","CK","ZH","ZH","WB","WB","IC","IC","RF","RF","NM","NM","LP","LP","FD","FD","XQ","XQ","WB","WB","ZH","ZH","CK","CK","XF","XF","OA","OA","CJ","CJ","RE","RE","IC","IC","RF","RF","NM","NM","LP","LP","FD","FD","XQ","XQ","WB","WB","ZH","ZH","CK","CK","XF","XF","OA","OA","CJ","CJ","RE","RE","XQ","XQ","FD","FD","ZZ"];

    loop {
      while search.len() > 0 {
        trace!("{:.>width$}", search.len(), width = search.len());
        let (cur, base) = search.pop_front().unwrap();

        let mut map = map.clone();
        for (to, (dist, from)) in shortest_path.iter().filter(|(to, _)| to.z == cur.z) {
          map.set(to.as_xy(), 'x');
        }
        for (p, _) in search.iter() {
          map.set(p.as_xy(), '*');
        }
        map.set(cur.as_xy(), 'O');


        for (next, dist, dlev) in &self.edges[&cur.as_xy()] {
          let cur_dist = base + dist;
          map.set(*next, '⁂');

          let next_level = match dlev {
            _ if !recurse => cur.z, // Disable level changes
            0 => cur.z,
            1 => {
              if cur.z == max_level {
                // We're trying to jump to a level we haven't visited yet
                // Save this jump point for when we decide we need to
                if held.iter().find(|(c, _)| cur == *c) == None {
                  debug!("Holding back going down from {} to {:?}", cur.z, next);
                  held.push_back((cur, base));
                }
                continue
              } else {
                debug!("Back down to {}", cur.z + 1);
                cur.z + 1
              }
            },
            -1 => {
              if cur.z == 0 {
                // Can't go up from 0
                continue;
              } else {
                debug!("Back up to {}", cur.z - 1);
                cur.z - 1
              }
            },
            _ => panic!("Can't move more than one level at once!")
          };

          let loc = Vec3::from_xyz(next.x, next.y, next_level);
          match shortest_path.get(&loc) {
            Some((dist, _)) if *dist > cur_dist => {
              shortest_path.insert(loc, (cur_dist, Some(cur)));
              if loc != to {
                search.push_back((loc, cur_dist));
              }
            },
            None => {
              shortest_path.insert(loc, (cur_dist, Some(cur)));
              search.push_back((loc, cur_dist));
              if loc != to {
                search.push_back((loc, cur_dist));
              }
            },
            _ => ()
          }
        }

        /*
        let mut named_path = Vec::new();
        let mut curpath = cur;
        while let Some((dist, Some(next))) = shortest_path.get(&curpath) {
          match self.named_nodes.get(&curpath.as_xy()) {
            Some(n) => named_path.push((n, curpath.z)),
            None => (),
          }
          curpath = *next;
        };
        let outpath = named_path.iter().rev();
        let matched = desired.iter().zip(outpath.clone()).fold(true, |matches, (expected, (out, _))| matches && expected.to_string() == out.to_string());

        if matched {
          println!("{}Layer {}:\n{}", "" /* util::cls() */, cur.z, map.draw(|c| *c.unwrap_or(&' ')));
          println!("{} Current named path: {}", matched, outpath.fold(String::new(), |mut s, (n, lev)| { s.push_str(&format!("{}:{}->", n, lev)); s }));

          let mut discard = String::new();
          io::stdin().read_line(&mut discard);
        }
        */
      }

      match shortest_path.get(&to) {
        Some((dist, Some(prev))) => {
          debug!("Found end! Reconstructing path...");
          let mut path: Vec<(u32, Vec3)> = Vec::new();
          let mut cur = prev;
          path.push((*dist, to));
          while let Some((dist, Some(next))) = shortest_path.get(cur) {
            trace!("Adding {:?} to path", cur);
            path.push((*dist, *cur));
            cur = next;
          };
          return path.iter().rev().cloned().collect();
        },
        Some((dist, None)) => panic!("Zero-length path!"),
        None if held.len() > 0 => {
          // No dice, add another level
          max_level += 1;
          search.append(&mut held);
        },
        None => panic!("Not found, and no exits found!?")
      };

      debug!("=========================");
      debug!("Advancing to level {}...", max_level);
      debug!("=========================");
    }
  }

  fn as_map(&self) -> tilemap::TileMap<char> {
    let mut map = tilemap::TileMap::new();
    for (from, dests) in &self.edges {
      for (to, dist, layer) in dests.iter() {
        if *dist > 1 {
          for x in from.x..=to.x {
            for y in from.y..=to.y {
              map.set(Vec2::from_xy(x, y), '.');
            }
          }
        }
        match layer {
          0 => {
            match map.get(from) { Some('.') | None => map.set(*from, 'o'), _ => () }
            match map.get(to) { Some('.') | None => map.set(*to, 'o'), _ => () }
          },
          -1 => {
            map.set(*from, '^');
            map.set(*to, 'v');
          },
          1 => {
            map.set(*from, 'v');
            map.set(*to, '^');
          },
          _ => (),
        }
      }
    }
    map
  }
}

pub fn run(input: String, part: Option<u8>) -> String {
  let graph = parse_map(&input);
  let path = graph.shortest_path(
    graph.unpaired_names[&['A','A']], graph.unpaired_names[&['Z','Z']], part == Some(2)
  );

  dbg!(&path);
  path.iter().last().unwrap().0.to_string()
}

fn parse_map(ascii: &str) -> FakeGraph {
  let lines = ascii.lines();
  let mut lines = lines.map(|l| l.chars().collect::<Vec<_>>());
  
  // This assumes we always have a portal on the top and bottom
  // I think this is okay?
  let mut prev = lines.clone();
  let mut cur = lines.clone().skip(1);
  let mut next = lines.clone().skip(2);

  let mut m = tilemap::TileMap::new();
  let mut graph = FakeGraph::new((
    lines.next().unwrap().len(),
    lines.count(),
  ));

  for (y, nline) in next.enumerate() {
    // TODO: Collecting each line twice is wasteful
    let pline = prev.next().unwrap();
    let line = cur.next().unwrap();
  
    // Ignore the first and last col, we look back/forward at those
    for (x, c) in line[..=(line.len()-2)].iter().enumerate().skip(1) {
      match c {
        ' ' => graph.clear_last(x, y),
        '#' => graph.clear_last(x, y),
        'A'..='Z' => {
          if pline[x].is_ascii_uppercase() && nline[x] == '.' {
            graph.add_named_node(x, y+1, [pline[x], *c]);
          } else if nline[x].is_ascii_uppercase() && pline[x] == '.' {
            graph.add_named_node(x, y-1, [*c, nline[x]]);
          } else if line[x-1].is_ascii_uppercase() && line[x+1] == '.' {
            graph.add_named_node(x+1, y, [line[x-1], line[x]]);
          } else if line[x+1].is_ascii_uppercase() && line[x-1] == '.' {
            graph.add_named_node(x-1, y, [line[x], line[x+1]]);
          }
        },
        '.' => {
          let vert = (pline[x] == '.') as u32 + (nline[x] == '.') as u32;
          let horiz = (line[x-1] == '.') as u32 + (line[x+1] == '.') as u32;
          if (vert + horiz) >= 3 {
            // Node
            graph.add_node(x,y);
          } else if vert == 2 {
            // Vert hall
            m.set(Vec2::from_xy(x as i32, y as i32), '|');
          } else if horiz == 2 {
            // Horiz hall
            m.set(Vec2::from_xy(x as i32, y as i32), '-');
          } else if vert == 1 && horiz == 1 {
            // Corner
            graph.add_node(x, y);
          } else if vert == 1 || horiz == 1 {
            // Dead end
            m.set(Vec2::from_xy(x as i32, y as i32), 'x');
          } else {
            // We don't care about inaccessible boxes
          }
        },
        _ => ()
      }
    }
  }

  graph
}

#[cfg(test)]
mod test {
  use super::*;

  fn init() {
      let _ = env_logger::builder().is_test(true).try_init();
  }

  #[test]
  fn parse() {
    let mut graph = parse_map(EX2);

    /*
    for (p, id) in &graph.nodes {
      m.set(*p, match id { Some(_) => 'O', None => 'o'});
    }
    println!("{}", m.draw(|x| match x { Some(&c) => c, None => ' ' }));
    */


    graph.dot_file("day20.dot");
    graph.prune_all();
    graph.dot_file("day20_pruned.dot");
    
    let path = graph.shortest_path(
        graph.unpaired_names[&['A','A']], graph.unpaired_names[&['Z','Z']], false
    );

    assert_eq!(58, path.iter().last().unwrap().0);
  }

  #[test]
  fn recurse() {
    init();

    let mut graph = parse_map(EX1);

    let path = graph.shortest_path(
        graph.unpaired_names[&['A','A']], graph.unpaired_names[&['Z','Z']], true
    );
    dbg!(&path);

    assert_eq!(26, path.iter().last().unwrap().0);

    let mut graph = parse_map(EX3);

    let path = graph.shortest_path(
        graph.unpaired_names[&['A','A']], graph.unpaired_names[&['Z','Z']], true
    );
    dbg!(&path);

    assert_eq!(396, path.iter().last().unwrap().0);


  }

  const EX1: &str =
"         A           
         A           
  #######.#########  
  #######.........#  
  #######.#######.#  
  #######.#######.#  
  #######.#######.#  
  #####  B    ###.#  
BC...##  C    ###.#  
  ##.##       ###.#  
  ##...DE  F  ###.#  
  #####    G  ###.#  
  #########.#####.#  
DE..#######...###.#  
  #.#########.###.#  
FG..#########.....#  
  ###########.#####  
             Z       
             Z       ";

  const EX2: &str =
"                   A               
                   A               
  #################.#############  
  #.#...#...................#.#.#  
  #.#.#.###.###.###.#########.#.#  
  #.#.#.......#...#.....#.#.#...#  
  #.#########.###.#####.#.#.###.#  
  #.............#.#.....#.......#  
  ###.###########.###.#####.#.#.#  
  #.....#        A   C    #.#.#.#  
  #######        S   P    #####.#  
  #.#...#                 #......VT
  #.#.#.#                 #.#####  
  #...#.#               YN....#.#  
  #.###.#                 #####.#  
DI....#.#                 #.....#  
  #####.#                 #.###.#  
ZZ......#               QG....#..AS
  ###.###                 #######  
JO..#.#.#                 #.....#  
  #.#.#.#                 ###.#.#  
  #...#..DI             BU....#..LF
  #####.#                 #.#####  
YN......#               VT..#....QG
  #.###.#                 #.###.#  
  #.#...#                 #.....#  
  ###.###    J L     J    #.#.###  
  #.....#    O F     P    #.#...#  
  #.###.#####.#.#####.#####.###.#  
  #...#.#.#...#.....#.....#.#...#  
  #.#####.###.###.#.#.#########.#  
  #...#.#.....#...#.#.#.#.....#.#  
  #.###.#####.###.###.#.#.#######  
  #.#.........#...#.............#  
  #########.###.###.#############  
           B   J   C               
           U   P   P               ";

  const EX3: &str =
"             Z L X W       C                 
             Z P Q B       K                 
  ###########.#.#.#.#######.###############  
  #...#.......#.#.......#.#.......#.#.#...#  
  ###.#.#.#.#.#.#.#.###.#.#.#######.#.#.###  
  #.#...#.#.#...#.#.#...#...#...#.#.......#  
  #.###.#######.###.###.#.###.###.#.#######  
  #...#.......#.#...#...#.............#...#  
  #.#########.#######.#.#######.#######.###  
  #...#.#    F       R I       Z    #.#.#.#  
  #.###.#    D       E C       H    #.#.#.#  
  #.#...#                           #...#.#  
  #.###.#                           #.###.#  
  #.#....OA                       WB..#.#..ZH
  #.###.#                           #.#.#.#  
CJ......#                           #.....#  
  #######                           #######  
  #.#....CK                         #......IC
  #.###.#                           #.###.#  
  #.....#                           #...#.#  
  ###.###                           #.#.#.#  
XF....#.#                         RF..#.#.#  
  #####.#                           #######  
  #......CJ                       NM..#...#  
  ###.#.#                           #.###.#  
RE....#.#                           #......RF
  ###.###        X   X       L      #.#.#.#  
  #.....#        F   Q       P      #.#.#.#  
  ###.###########.###.#######.#########.###  
  #.....#...#.....#.......#...#.....#.#...#  
  #####.#.###.#######.#######.###.###.#.#.#  
  #.......#.......#.#.#.#.#...#...#...#.#.#  
  #####.###.#####.#.#.#.#.###.###.#.###.###  
  #.......#.....#.#...#...............#...#  
  #############.#.#.###.###################  
               A O F   N                     
               A A D   M                     ";
}

