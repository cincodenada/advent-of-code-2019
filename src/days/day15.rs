use crate::util::{self, intcode};
use crate::util::geom::{self, Vec2, Direction};

use std::collections::HashMap;
use std::cmp;

use num_traits::{FromPrimitive, ToPrimitive};

#[derive(Eq, PartialEq, Debug)]
enum DroidStatus {
  WALL,
  MOVED,
  GOAL
}

const cost_chars: [char; 6] = [
  '․','⁚','⁖','⁘','⁙','⁜',
];
#[derive(Eq, PartialEq, Debug, Clone)]
struct MapCell {
  loc: Vec2,
  entry: Option<Direction>, // Start is None
  cost_to: u32,
  cost_from: u32,
}
impl MapCell {
}
type CostMap = HashMap<Vec2, MapCell>;

trait DroidParam {
  fn as_param(&self) -> i128;
}
impl DroidParam for Direction {
  fn as_param(&self) -> i128 {
    match self {
      Self::NORTH => 1,
      Self::SOUTH => 2,
      Self::WEST => 3,
      Self::EAST => 4,
    }
  }
}

struct Droid {
  prog: intcode::Program,
  dir: Direction,
  pos: Vec2,
  map: CostMap,
  search_map: HashMap<Vec2, u32>,
}
impl Droid {
  fn new(input: &str) -> Droid {
    Droid {
      prog: intcode::Program::from_csv(input),
      dir: Direction::NORTH,
      pos: Vec2::new(),
      map: HashMap::new(),
      search_map: HashMap::new(),
    }
  }

  fn go(&mut self) -> DroidStatus {
    trace!("Going: {:?} from {:?}", self.dir, self.pos);
    self.prog.put(self.dir.as_param());
    self.prog.resume();
    match self.prog.get() {
      Some(0) => {
        self.search_map.insert(self.pos + self.dir.as_vec(), std::u32::MAX);
        DroidStatus::WALL
      }
      Some(1) => {
        self.pos += self.dir.as_vec();
        self.search_map.insert(self.pos, 1);
        DroidStatus::MOVED
      },
      Some(2) => {
        self.pos += self.dir.as_vec();
        self.search_map.insert(self.pos, 2);
        DroidStatus::GOAL
      },
      Some(d) => panic!("Unknown status {}!", d),
      None => panic!("No status returned!")
    }
  }

  fn rotate(&mut self, rot_dir: i8) {
    self.dir = Direction::from_i8((self.dir.to_i8().expect("Invalid rotation!")+4+rot_dir)%4).expect("Invalid direction!");
  }

  fn go_dir(&mut self, dir: Direction) -> DroidStatus {
    self.dir = dir;
    self.go()
  }
  
  fn draw_search(&self) {
    println!("{}", util::draw_map(&self.search_map, |pri| match pri {
      Some(&std::u32::MAX) => '#',
      Some(&0) => 'o',
      Some(&1) => '.',
      Some(&2) => '!',
      Some(_) => '?',
      None => ' ',
    }, vec![]));
  }
  fn draw(&self, overlays: Vec<(Vec2, char)>) {
    println!("{}{}", util::cls(), util::draw_map(&self.map, |pri| match pri {
      Some(c) if c.cost_to == std::u32::MAX => '🔲',
      Some(c) if c.cost_to == 0 => '🏁',
      Some(_) => '◾',
      None => '⬛',
    }, overlays));
  }
}



pub fn run(input: String, part: Option<u8>) -> String {
  let mut artoo = Droid::new(&input);
  //artoo.map.insert(Vec2::new(), 0);

  let goal = find_target(&mut artoo);
  artoo.draw_search();
  let (path, max_cost) = find_path(&mut artoo, &goal, &Vec2::new());
  let path = path.unwrap();
  let mut overlay: Vec<_> = path.iter().map(|&v| (v, '💚')).collect();
  overlay.push((Vec2::new(), '🏁'));
  overlay.push((goal, '🏆'));
  artoo.draw(overlay);

  match part {
    Some(1) => (path.len()-1).to_string(), // The first spot isn't a "move"
    Some(2) | None => max_cost.to_string(),
    Some(p) => panic!("Unknown part {}!", p)
  }
}

fn find_target(droid: &mut Droid) -> Vec2 {
  // Find a wall
  while match droid.go_dir(Direction::NORTH) {
      DroidStatus::MOVED => true,
      DroidStatus::WALL => false,
      DroidStatus::GOAL => return droid.pos,
  } {}

  loop {
    match droid.go() {
      DroidStatus::MOVED => {
        droid.rotate(-1);
        /*
        match droid.map.get(&droid.pos) {
          Some(_) => {droid.rotate(2); droid.go(); droid.rotate(-1)},
          None => droid.rotate(-1),
        }
        */
      }
      DroidStatus::WALL => droid.rotate(1),
      DroidStatus::GOAL => return droid.pos,
    }
    //droid.draw();
  }
}

fn find_path(droid: &mut Droid, from: &Vec2, to: &Vec2) -> (Option<Vec<Vec2>>, u32) {
  let mut shortest = None;
  let mut max_cost = 0;

  let mut frontier = Vec::new();
  let start = MapCell {
    loc: *from,
    entry: None,
    cost_to: 0,
    cost_from: from.manhattan_dist(to),
  };
  droid.map.insert(*from, start.clone());
  frontier.push(start);

  while frontier.len() > 0 {
    frontier.sort_by(|a, b| (b.cost_to + b.cost_from).cmp(&(a.cost_to+a.cost_from)));
    let cur = frontier.pop().unwrap();
    if cur.loc == *to {
      // We found it! Build our path
      let mut path = Vec::new();
      let mut step = cur.loc;
      while match &droid.map[&step] {
        c if c.cost_to > 0 => {
          path.push(step.clone());
          step += c.entry.unwrap().reverse().as_vec();
          true
        },
        _ => {
          path.push(step.clone());
          // We made it back to the start/end
          false
        }
      } {}
      path.reverse();
      shortest = Some(path)
    }
    /*
    let mut overlay: Vec<_> = frontier.iter().map(|c| (c.loc, '💭')).collect();
    overlay.push((cur.loc, '🔍'));
    overlay.push((*to, '🏆'));
    droid.draw(overlay);
    io::stdout().flush();
    */

    if droid.pos != cur.loc {
      let entry_dir = cur.entry.unwrap();
      if droid.pos + entry_dir.as_vec() == cur.loc {
        assert_ne!(DroidStatus::WALL, droid.go_dir(entry_dir));
        trace!("Moved {:?} to {:?}", entry_dir, droid.pos);
      } else {
        debug!("Backtracking from {:?} to {:?}", droid.pos, cur.loc);
        // Backtrack, whee
        // Gather positions from target to start
        let mut from_goal = cur.loc;
        let mut path = Vec::new();
        while match &droid.map[&from_goal] {
          c if c.cost_to > 0 => {
            path.push(from_goal.clone());
            from_goal += c.entry.unwrap().reverse().as_vec();
            true
          },
          _ => false
        } {}
        let mut it = path.iter().rev();
        while { it = path.iter().rev(); it.find(|&&loc| loc == droid.pos) == None } {
          let dir = droid.map[&droid.pos].entry.unwrap().reverse();
          assert_ne!(DroidStatus::WALL, droid.go_dir(dir));
        }
        debug!("Found branch, proceeding...");
        while match it.next() {
          Some(loc) => {
            let dir = droid.map[loc].entry.unwrap();
            assert_ne!(DroidStatus::WALL, droid.go_dir(dir));
            trace!("Went {:?} to {:?}", dir, droid.pos);
            true
          },
          None => false
        } {}
      }
    }
    //println!("Options: {:?}", frontier);
    debug!("Checking on {:?} at {:?}", cur, droid.pos);
    for d in geom::DIRECTIONS.iter() {
      let add = cur.loc + d.as_vec();
      if droid.map.get(&add) == None {
        trace!("Probing {:?} at ({},{})", d, add.x, add.y);
        match droid.go_dir(*d) {
          DroidStatus::WALL => {
            droid.map.insert(add, MapCell{
              loc: add,
              entry: Some(*d),
              cost_to: std::u32::MAX,
              cost_from: 0,
            });
            trace!("Adding wall at {:?}", add);
          },
          DroidStatus::MOVED => {
            let cell = MapCell {
              loc: add,
              entry: Some(*d),
              cost_to: cur.cost_to + 1,
              cost_from: add.manhattan_dist(to),
            };
            max_cost = cmp::max(max_cost, cell.cost_to);
            frontier.push(cell.clone());
            droid.map.insert(add, cell);
            trace!("Adding candidate at {:?}: {:?}", add, frontier.iter().last());
            assert_ne!(DroidStatus::WALL, droid.go_dir(d.reverse()));
          },
          DroidStatus::GOAL => panic!("Shouldn't return to start!")
        }
      } else {
        trace!("{:?} already mapped", d);
      }
    }
  }

  (shortest, max_cost)
}

#[cfg(test)]
mod test {
}
