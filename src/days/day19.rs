use crate::util::{intcode, tilemap, geom::Vec2};

trait TractorDroid {
  fn is_pulled(&mut self, x: i32, y: i32) -> Option<i128>;

  fn find_left_edge(&mut self, start_x: i32, y: i32, range: i32) -> Option<i32>;
  fn find_right_edge(&mut self, left_edge: i32, y: i32, prev_width: i32) -> Option<i32>;
}

impl TractorDroid for intcode::Program {
  fn is_pulled(&mut self, x: i32, y: i32) -> Option<i128> {
    self.put(x as i128);
    self.put(y as i128);
    self.run();
    self.get()
  }

  fn find_left_edge(&mut self, start_x: i32, y: i32, range: i32) -> Option<i32> {
    trace!("Finding left edge for {}, searching {} starting at {}", y, range, start_x);
    for x in start_x..(start_x+range) {
      if self.is_pulled(x, y) == Some(1) {
        trace!("Found at {}", x);
        return Some(x);
      }
    }
    trace!("Edge not found");
    None
  }

  fn find_right_edge(&mut self, left_edge: i32, y: i32, prev_width: i32) -> Option<i32> {
    trace!("Finding right edge for {} starting at {}+{}-1", y, left_edge, prev_width);
    for x in (left_edge+prev_width-1).. {
      if self.is_pulled(x, y) == Some(0) {
        return Some(x-1);
      }
    }
    None
  }
}

pub fn run(input: String, part: Option<u8>) -> String {
  let mut prog = intcode::Program::from_csv(&input);

  let mut map = tilemap::TileMap::new();
  map.set_xy(0,0,1); // Right next to the tractor is always on

  let answer = match part {
    Some(1) => {
      find_beam(&mut prog, &mut map, 50);
      map.count(1) + map.count(2)
    }
    Some(2) | None => {
      let ship_size = 100;
      let ship_pos = find_ship_spot(&mut prog, &mut map, ship_size);

      map.add_overlay(Vec2::from_xy(ship_pos.0 as i32, ship_pos.1 as i32), 'O');

      (ship_pos.0*10_000 + ship_pos.1) as usize
    },
    Some(p) => panic!("Unknown part {}!", p)
  };

  /*
  println!("{}", map.draw(|v| match v {
    Some(0) => '.',
    Some(1) => '#',
    Some(2) => '|',
    Some(_) => '?',
    None => ' ',
  }));
  */

  answer.to_string()
}

fn find_ship_spot(prog: &mut intcode::Program, map: &mut tilemap::TileMap<u32>, ship_size: u32) -> (u32, u32) {
  // Pick a start row suitably far down
  let start_row = match find_row(prog, map, ship_size as i32, ship_size as i32/2, 1) {
    Some(edges) => {
      let width = edges.1 - edges.0 + 1;
      let rows_per_width = ship_size as i32/width;
      let min_width = (ship_size + ship_size/2) as i32;
      min_width*rows_per_width
    },
    None => panic!("Couldn't find start row!")
  };

  let mut first_possible_fit_row = None;
  let mut prev_edge = start_row/2;
  let mut prev_width = 1;
  for y in start_row.. {
    match find_row(prog, map, y, prev_edge, prev_width) {
      Some(edges) => {
        prev_edge = edges.0;
        prev_width = edges.1 - edges.0 + 1;

        match first_possible_fit_row {
          Some(fy) if y < fy => (),
          Some(_) => {
            let opposite_corner = Vec2::from_xy(
              edges.0 + ship_size as i32 - 1,
              y - ship_size as i32 + 1
            );
            match map.get(&opposite_corner) {
              Some(&n) if n > 0 => {
                debug!("Found space for ship on row {}, between {} and {}", y, edges.0, edges.1);
                return (edges.0 as u32, y as u32 - ship_size + 1)
              },
              _ => {
                trace!("{:?} isn't in tractor beam, row {} is too small", opposite_corner, y);
              }
            }
          },
          None => {
            if (edges.1 - edges.0 + 1) >= ship_size as i32 {
              // We could be cleverer here, but this is conservative
              first_possible_fit_row = Some(y + ship_size as i32 - 1);
            }
          }
        }
      },
      None => if y % 2 == 0 { prev_edge += 1 }
    }
  }
  panic!("Exited infinite loop?!")
}

fn find_beam(prog: &mut intcode::Program, map: &mut tilemap::TileMap<u32>, rows: i32) {
  let mut prev_edge = 0;
  let mut prev_width = 1;
  for y in 1..rows {
    match find_row(prog, map, y, prev_edge, prev_width) {
      Some(edges) => {
        prev_edge = edges.0;
        prev_width = edges.1 - edges.0 + 1;
      },
      None => if y % 2 == 0 { prev_edge += 1 }
    }
  }
}

fn find_row(prog: &mut intcode::Program, map: &mut tilemap::TileMap<u32>, y: i32, prev_edge: i32, prev_width: i32) -> Option<(i32, i32)> {
  let left_edge = match prog.find_left_edge(prev_edge, y, y/2) {
    Some(edge) => edge,
    None => return None
  };

  match prog.find_right_edge(left_edge, y, prev_width) {
    Some(right_edge) => {
      for x in (left_edge+1)..right_edge {
        map.set_xy(x, y, 1);
      }
      map.set_xy(left_edge, y, 2);
      map.set_xy(right_edge, y, 2);

      return Some((left_edge, right_edge));
    },
    None => panic!("Right edge not found!")
  }
}


#[cfg(test)]
mod test {
}
