use std::collections::HashMap;
use crate::util::intcode;
use crate::util::geom::{self, Vec2, Direction};
use crate::util::tilemap::TileMap;
use std::char;

use num_traits::{FromPrimitive, ToPrimitive};

pub fn robot_dir(c: char) -> Option<Direction> {
  match ['^','>','v','<','X'].iter().position(|&r| c == r) {
    Some(idx) if idx < 4 => Direction::from_usize(idx),
    Some(4) => panic!("Robot has fallen off!"),
    Some(_) => panic!("Robot is deformed!"),
    None => None
  }
}

pub fn run(input: String, part: Option<u8>) -> String {
  let mut prog = intcode::Program::from_csv(&input);

  prog.run();
  let mut x = 0;
  let mut y = 0;
  let mut map = TileMap::new();
  let mut robot_pos = Vec2::new();
  loop {
    match prog.get() {
      Some(10) => { x = 0; y += 1 }
      Some(c) => {
        match char::from_u32(c as u32) {
          Some(t) => {
            if robot_dir(t) != None {
              robot_pos = Vec2::from_xy(x, y);
            }
            map.set_xy(x, y, t);
          },
          None => panic!("Couldn't convert char {}", c)
        }
        x += 1;
      },
      None => break
    }
  }

  let intersections = map.iter().filter(|(&loc, &c)| {
    if c != '#' { return false }
    for d in &geom::DIRECTIONS {
      match map.get(&(loc + d.as_vec())) {
        Some('#') => (),
        _ => return false,
      }
    }
    true
  }).map(|(&loc, &c)| loc).collect::<Vec<_>>();

  for &loc in &intersections {
    map.add_overlay(loc, 'O');
  }
  map.add_overlay(robot_pos, '🤖');
  
  let checksum: i32 = intersections.iter().map(|loc| loc.x * loc.y).sum();

  println!("{}", map.draw(|c| *c.unwrap()));

  let path = find_path(&map, &robot_pos);

  let ngram_sets = build_set(3, 2, 5);
  let mut ngrams = HashMap::new();
  for len in 2..=5 {
    let mut cur = get_ngrams(&path, len);
    cur.sort_by_key(|p| p.1);
    ngrams.insert(len, cur);
  }
  dbg!(&ngrams[&3]);
  for set in ngram_sets {
    
  }

  dbg!(path.len());

  match part {
    Some(1) => checksum.to_string(),
    Some(2) | None => String::new(),
    Some(p) => panic!("Unknown part {}!", p)
  }
}

fn build_set(len: u32, low: u32, high: u32) -> Vec<Vec<u32>> {
  if len == 1 {
    (low..=high).map(|v| vec![v]).collect()
  } else {
    let mut vecs: Vec<Vec<u32>> = Vec::new();
    for s in build_set(len-1, low, high) {
      for n in s[s.len()-1]..=high {
        let mut cur = s.clone();
        cur.push(n);
        vecs.push(cur)
      }
    }
    vecs
  }
}

fn get_ngrams<'a, T>(list: &Vec<T>, length: usize) -> Vec<(Vec<T>, u32)>
where T: std::hash::Hash + Eq + Copy + std::fmt::Debug {
  let mut counts: HashMap<Vec<T>, u32> = HashMap::new();
  let mut iters = (0..length).map(|l| list.iter().skip(l).cloned()).collect::<Vec<_>>();
  for _ in 0..(list.len() - length) {
    let ngram = iters.iter_mut().map(|i| i.next().unwrap()).collect::<Vec<T>>();
    match counts.get_mut(&ngram) {
      Some(count) => *count += 1,
      None => { counts.insert(ngram, 1); },
    }
  }
  counts.drain().map(|t| t).collect()
}

fn find_path(map: &TileMap<char>, start: &Vec2) -> Vec<(char, u32)> {
  let mut path = Vec::new();
  let mut dir = robot_dir(*map.get(start).unwrap()).unwrap();
  let mut cur = *start;
  let mut last = *start;
  let mut cur_move = 0;
  let mut last_rot = 'X';

  println!("Finding path...");

  let find_dir = |cur: &Vec2, last: &Vec2| -> Option<Direction> {
    // Rotate and find our next path
    for d in &geom::DIRECTIONS {
      let next = *cur + d.as_vec();
      match map.get(&next) {
        Some('#') if next != *last => return Some(*d),
        _ => ()
      }
    }
    None
  };

  loop {
    let next = cur + dir.as_vec();
    let last = cur - dir.as_vec();
    match map.get(&next) {
      Some('#') => {
        cur_move += 1;
        cur = next
      },
      _ => {
        // Append last path
        if !(last_rot == 'X' && cur_move == 0) {
          if last_rot == 'U' {
            path.push(('L',0));
            path.push(('L',cur_move));
          } else {
            path.push((last_rot, cur_move));
          }
        }
        cur_move = 0;

        let new_dir = match find_dir(&cur, &last) {
          Some(d) => d,
          None => return path
        };

        last_rot = match (new_dir.to_i8().unwrap() - dir.to_i8().unwrap() + 4) % 4 {
          0 => '?',
          1 => 'R',
          2 => 'U',
          3 => 'L',
          _ => '%',
        };
        dir = new_dir;
      }
    }
  }
}

#[cfg(test)]
mod test {
  use super::*;

  #[test]
  fn pathfinding() {
    let mut map = TileMap::new();
    let mut robot_pos = Vec2::new();

    for (y, l) in EX1.lines().enumerate() {
      for (x, c) in l.chars().enumerate() {
        let x = x as i32;
        let y = y as i32;
        map.set_xy(x,y,c);
        if robot_dir(c) != None {
          robot_pos = Vec2::from_xy(x, y);
        }
      }
    }

    assert_eq!(
      vec![('R',8),('R',8),('R',4),('R',4),('R',8),('L',6),('L',2),('R',4),('R',4),('R',8),('R',8),('R',8),('L',6),('L',2)],
      find_path(&map, &robot_pos));
  }

  #[test]
  fn sets() {
    assert_eq!(vec![
      vec![0,0],
      vec![0,1],
      vec![1,1],
    ], build_set(2, 0, 1));

    assert_eq!(vec![
      vec![0,0],
      vec![0,1],
      vec![0,2],
      vec![1,1],
      vec![1,2],
      vec![2,2],
    ], build_set(2, 0, 2));

    assert_eq!(vec![
      vec![0,0,0],
      vec![0,0,1],
      vec![0,0,2],
      vec![0,1,1],
      vec![0,1,2],
      vec![0,2,2],
      vec![1,1,1],
      vec![1,1,2],
      vec![1,2,2],
      vec![2,2,2],
    ], build_set(3, 0, 2));
  }

  const EX1: &str =
"#######...#####
#.....#...#...#
#.....#...#...#
......#...#...#
......#...###.#
......#.....#.#
^########...#.#
......#.#...#.#
......#########
........#...#..
....#########..
....#...#......
....#...#......
....#...#......
....#####......";
}
